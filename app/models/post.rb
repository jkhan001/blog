class Post < ActiveRecord::Base
  # comments are associated with specific posts
  has_many :comments, dependent: :destroy
  # Validates fiedl entry on sever-side
  validates_presence_of :title
  validates_presence_of :body
end
